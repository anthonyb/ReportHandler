package main.report;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import main.Main;
import main.report.post.Post;

public class Report {
	
	private int postNum;
	
	private String urlStr, postTitle;
	
	private List<Post> posts;
	
	public Report(String urlStr) {
		
		this.postNum = -1;
		
		this.urlStr = urlStr;
		
		this.posts = new ArrayList<Post>();
		
		String currentPostMessage = "", currentPostAttachment = "";
		
		try {
			
			URL url = new URL(urlStr);
			
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			
			boolean nextLineIsMessage = false, nextLineIsAttachment = false;
			
			String str;
			
			while ((str = in.readLine()) != null) {
				
				if (this.has(str, "<div class=\"titleBar\">") != -1) {
					
//					Read the space.
					str = in.readLine();
//					Read the <h1>Title</h1> line.
					str = in.readLine();
					
					String h1Sub = str.substring(str.indexOf("<h1>") + "<h1>".length());
					
					this.postTitle = h1Sub.substring(0, h1Sub.indexOf("</h1>"));
				}
				
				if (this.has(str, "<li id=\"post-") != -1) {
					
					String poster = str.substring(str.indexOf("data-author=\"") + "data-author=\"".length());
					poster = poster.substring(0, poster.indexOf("\""));
					
					nextLineIsMessage = false;
					this.posts.add(++this.postNum, new Post(poster));
				}
				
				if (nextLineIsMessage && this.postNum >= 0) {
					
					if (this.has(str, "<div class=\"messageTextEndMarker\">&nbsp;</div>") != -1) {
						
						nextLineIsMessage = false;
						
						this.posts.get(this.postNum).setMessage(currentPostMessage);
					}
					
					currentPostMessage += str + Main.HTML_NEW_LINE;
				}
				
				if (nextLineIsAttachment && this.postNum >= 0) {
					
					if (this.has(str, "</ul>") != -1) {
						
						nextLineIsAttachment = false;
						
						this.posts.get(this.postNum).setAttachments(currentPostAttachment);
					}
					
					if (this.has(str, "<a ") != -1) {
						
						currentPostAttachment += str;
					}
				}
				
				if (this.has(str, "<blockquote class=\"messageText SelectQuoteContainer ugc baseHtml\">") != -1) { //Gets a message
					
					nextLineIsMessage = true;
					currentPostMessage = "";
				}
				
				if (this.has(str, "<ul class=\"attachmentList SquareThumbs\"") != -1) {
					
					nextLineIsAttachment = true;
					currentPostAttachment = "";
				}
			}
			
			in.close();
		} catch (MalformedURLException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	private int has(String str, String check) {
		
		int index = 0;
		
		if ((index = str.indexOf(check)) != -1) {
			
			return index;
		}
		
		return -1;
	}
	
	public String getUrlStr() {
		
		return this.urlStr;
	}
	
	@Override
	public String toString() {
		
		Post original = this.posts.get(0);
		
		String ret = "<b>URL</b>: " + this.urlStr + Main.HTML_NEW_LINE
			+ "<b>Title</b>: " + this.postTitle + Main.HTML_NEW_LINE
			+ "<b>Poster</b>: " + original.getPoster() + Main.HTML_NEW_LINE
			+ "<b>Message</b>: " + original.getMessage() + Main.HTML_NEW_LINE
			+ original.getTaggedUsersAsString()
			+ original.getAttachedUrlsAsString()
			+ Main.HTML_NEW_LINE + Main.HTML_NEW_LINE
		;
		
		if (this.posts.size() > 1) {
			
			ret += "<strike>---</strike>   RESPONSES   <strike>---</strike>" + Main.HTML_NEW_LINE;
		}
		
		for (int i = 1; i < this.posts.size(); i++) {
			
			Post post = this.posts.get(i);
			
			ret += 	"===================================" + Main.HTML_NEW_LINE
				+	"<b>Poster</b>: " + post.getPoster() + Main.HTML_NEW_LINE
				+	"<b>Message</b>: " + post.getMessage() + Main.HTML_NEW_LINE
				+	post.getTaggedUsersAsString()
				+	post.getAttachedUrlsAsString()
				+	Main.HTML_NEW_LINE
			;
		}
		
		return ret;
	}
	
	public String getPoster() {
		
		return this.posts.get(0).getPoster();
	}
}