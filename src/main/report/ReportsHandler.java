package main.report;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import main.profile.Profile;

public class ReportsHandler {
	
	private List<Report> reports;
	
	public ReportsHandler() throws IOException {
		
		this.reports = new ArrayList<Report>();
		
		this.refill();
	}
	
	public void refill() throws IOException {
		
		this.refill("LINK REDACTED");
	}
	
	public void refill(String url) throws IOException {
		
		Document doc = Jsoup.connect(url).timeout(0).get();
		
		Elements li_s = doc.select("li");
		
		List<Element> reports = new ArrayList<Element>();
		
		for (Element e : li_s) {
			
			String className = e.className();
			
			if (e.id().startsWith("thread") && !className.contains("locked") && !className.contains("sticky") && className.contains("visible")) {
				
				reports.add(e);
			}
		}
		
		if (reports.size() == 0) {
			
			if (url.equals("LINK REDACTED")) {
				
				this.refill("LINK REDACTED");
				return;
			}
			
			int page = Integer.parseInt("" + url.charAt(url.length() - 1)) + 1;
			
			if (page < Profile.maxPage) {
				
				this.refill(url.substring(0, url.length() - 1) + page);
			}
		}
		
		for (Element e : reports) {
			
			Elements links = e.select("a[href]");
			
//			The report URL comes up multiple times under the thread in the list.
			List<String> alreadyUsedReports = new ArrayList<String>();
			
			for (Element link : links) {
				
				String postLink = link.attr("href");
				
				if (postLink.startsWith("threads/")) {
					
					String reportLink = "LINK REDACTED" + postLink;
					
					if (!alreadyUsedReports.contains(reportLink)) {
						
						this.reports.add(new Report(reportLink));
						alreadyUsedReports.add(reportLink);
					}
				}
			}
		}
	}
	
	public List<Report> getReportsDirectly() {
		
		return this.reports;
	}
}