package main.report.post;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import main.Main;
import main.profile.Profile;

public class Post {
	
	private String poster, message;
	
	private List<String> taggedUsers, attachedImageUrls;
	
	public Post(String poster) {
		
		this.taggedUsers = new ArrayList<String>();
		this.attachedImageUrls = new ArrayList<String>();
		
		this.poster = poster;
		this.message = "N/A";
	}
	
	public void setMessage(String message) {
		
		this.message = message.substring(0, message.length() - Main.HTML_NEW_LINE.length());
		
		this.message = this.message
			.replaceAll("    ", "")
			.replaceAll("<br /><br />", Main.HTML_NEW_LINE)
			.replaceAll("•", "\u2022")
			.replaceAll("&#039;", "\'")
			.replaceAll("&quot;", "\"")
		;
		
		int index;
		while ((index = this.message.indexOf("<a href=\"LINK REDACTED")) != -1) {
			
			String subIndex = this.message.substring(index);
			
			String htmlStatement = subIndex.substring(0, subIndex.indexOf("</a>"));
			
			String atuser = htmlStatement.substring(htmlStatement.indexOf('>') + 1);
			
			if (!this.taggedUsers.contains(atuser)) {
				
				this.taggedUsers.add(atuser);
			}
			
			this.message = this.message.substring(0, index) + atuser + this.message.substring(index + htmlStatement.length() + "</a>".length());
		}
		
		while ((index = this.message.indexOf("<iframe")) != -1) {
			
			String subIndex = this.message.substring(index);
			
			String htmlStatement = subIndex.substring(0, subIndex.indexOf("</iframe>"));
			
			String srcString = htmlStatement.substring(htmlStatement.indexOf("src=\"") + "src=\"".length());
			
			String videoLink = srcString.substring(0, srcString.indexOf("\""));
			
			videoLink = "https://youtu.be/" + videoLink.substring(videoLink.indexOf("embed/") + "embed/".length(), videoLink.indexOf("?"));
			
			this.message = this.message.substring(0, index) + "[VIDEO url= " + videoLink + "]" + this.message.substring(index + htmlStatement.length() + "</iframe>".length());
		}
		
		while ((index = this.message.indexOf("<img src=\"styles/default/xenforo/clear.png\"")) != -1) {
			
			String subIndex = this.message.substring(index);
			
			String htmlStatement = subIndex.substring(0, subIndex.indexOf(" />"));
			
			String altStr = htmlStatement.substring(htmlStatement.indexOf("alt=\"") + "alt=\"".length());
			
			String emoji = altStr.substring(0, altStr.indexOf("\""));
			
			this.message = this.message.substring(0, index) + "( " + emoji + " )" + this.message.substring(index + htmlStatement.length() + " />".length());
		}
		
		while ((index = this.message.indexOf("<div class=\"bbCodeBlock bbCodeQuote\"")) != -1) {
			
			String subIndex = this.message.substring(index);
			
			String htmlStatement = subIndex.substring(0, subIndex.indexOf("</aside>"));
			
			String dataAuthorString = htmlStatement.substring(htmlStatement.indexOf("data-author=\"") + "data-author=\"".length());
			
			String authorStr = dataAuthorString.substring(0, dataAuthorString.indexOf("\""));
			
			String quoteHtmlStatement = htmlStatement.substring(htmlStatement.indexOf("<div class=\"quote\">") + "<div class=\"quote\">".length());
			
			String quote = quoteHtmlStatement.substring(0, quoteHtmlStatement.indexOf("</div>"));
			
			this.message = this.message.substring(0, index) + "[QUOTE \"" + quote + "\" - " + authorStr + "]" + Main.HTML_NEW_LINE + this.message.substring(index + htmlStatement.length() + "</aside></div>".length());
		}
	}
	
	public void setAttachments(String attachmentStr) {
		
		int index;
		while ((index = attachmentStr.indexOf("<a href=\"attachments/")) != -1) {
			
			String subIndex = attachmentStr.substring(index);
			
			String htmlStatement = subIndex.substring(0, subIndex.indexOf("</a>"));
			
			String hrefString = htmlStatement.substring("<a href=\"".length());
			
			String link = "LINK REDACTED" + hrefString.substring(0, hrefString.indexOf("\""));
			
			if (!this.attachedImageUrls.contains(link)) {
				
				this.attachedImageUrls.add(link);
			}
			
			attachmentStr = attachmentStr.substring(0, index) + attachmentStr.substring(index + htmlStatement.length() + "</a>".length());
		}
	}
	
	public String getPoster() {
		
		return this.poster;
	}
	
	public String getMessage() {
		
		return this.message;
	}
	
	public String getTaggedUsersAsString() {
		
		String ret = this.taggedUsers.size() > 0 ? "<b>Tagged</b>: " + Arrays.toString(this.taggedUsers.toArray()) + Main.HTML_NEW_LINE : "";
		
		for (String name : this.taggedUsers) {
			
			if (name.equalsIgnoreCase(Profile.forumsUsername)) {
				
				ret += "***   <b>YOU WERE TAGGED IN THIS POST</b>   ***" + Main.HTML_NEW_LINE;
			}
		}
		
		return ret;
	}
	
	public String getAttachedUrlsAsString() {
		
		return this.attachedImageUrls.size() > 0 ? "<b>Attached Images</b>: " + Arrays.toString(this.attachedImageUrls.toArray()) + Main.HTML_NEW_LINE : "";
	}
	
	public List<String> getTaggedUsers() {
		
		return this.taggedUsers;
	}
}