package main.shutdownhook;

import java.io.IOException;

import javax.swing.JOptionPane;

import main.profile.Profile;

public class ShutdownHook extends Thread {
	
	@Override
	public void run() {
		
		try {
			
			Profile.logout();
		} catch (IOException e) {
			
			JOptionPane.showMessageDialog(null, "An error occurred when logging out.  Please check the forums to see if you\'re still logged in!");
		}
	}
}