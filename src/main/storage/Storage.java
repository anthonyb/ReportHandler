package main.storage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import main.Main;

public class Storage {
	
	private static final String 
		
		FILE_SEPARATOR = System.getProperty("file.separator"),
		FILE_DIR = System.getProperty("user.home") + Storage.FILE_SEPARATOR + "Report_Handler",
		FILE_LOC = Storage.FILE_DIR + Storage.FILE_SEPARATOR + "info.txt";
	;
	
	public static void createFile() throws IOException {
		
		new File(Storage.FILE_DIR).mkdir();
		new File(Storage.FILE_LOC).createNewFile();
	}
	
	public static boolean hasFile() {
		
		if (new File(Storage.FILE_LOC).exists()) {
			
			return true;
		}
		
		return false;
	}
	
	public static File getFile() {
		
		return new File(Storage.FILE_LOC);
	}
	
	public static void write(String username, String password, String forumsUsername, String forumsPassword) throws IOException {
		
		FileWriter fileWriter = new FileWriter(Storage.getFile());
		
		fileWriter.write("Username:" + username);
		fileWriter.append(Main.NEW_LINE + "Password:" + password);
		fileWriter.append(Main.NEW_LINE + "fUsername:" + forumsUsername);
		fileWriter.append(Main.NEW_LINE + "fPassword:" + forumsPassword);
		
		fileWriter.close();
	}
	
	public static String[] getInformation() throws FileNotFoundException {
		
		Scanner scanner = new Scanner(Storage.getFile());
		
		String[] info = new String[]{scanner.nextLine(), scanner.nextLine(), scanner.nextLine(), scanner.nextLine()};
		
		scanner.close();
		
		if (!info[0].startsWith("Username:")) {
			
			throw new FileNotFoundException();
		}
		
		if (!info[1].startsWith("Password:")) {
			
			throw new FileNotFoundException();
		}
		
		if (!info[2].startsWith("fUsername:")) {
			
			throw new FileNotFoundException();
		}
		
		if (!info[3].startsWith("fPassword:")) {
			
			throw new FileNotFoundException();
		}
		
		return info;
	}
}