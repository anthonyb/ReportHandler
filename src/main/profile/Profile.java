package main.profile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import main.report.Report;

public class Profile {
	
	public static int maxPage = 25;
	
	public static String
		
		username,
		password,
		forumsUsername,
		forumsPassword
	;
	
	public static Map<String, String> banPageCookies, forumsPageCookies;
	
	public static void init(String banUsername, String banPassword, String forumsUsername, String forumsPassword, String twoStepCode) throws IOException {
		
		Profile.setBanPageUsername(banUsername);
		Profile.setBanPagePassword(banPassword);
		Profile.setForumsUsername(forumsUsername);
		Profile.setForumsPassword(forumsPassword);
		
		Connection.Response banPageLoginForm = Jsoup.connect("LINK REDACTED")
			.method(Connection.Method.GET)
			.execute()
		;
		
		Profile.banPageCookies = banPageLoginForm.cookies();
		
		banPageLoginForm = Jsoup.connect("LINK REDACTED")
			
			.data("username", Profile.username)
			.data("password", Profile.password)
			.cookies(Profile.banPageCookies)
			.method(Method.POST)
			.execute()
		;
		
		Profile.banPageCookies.putAll(banPageLoginForm.cookies());
		
		Connection.Response forumsLoginForm = Jsoup.connect("LINK REDACTED")
			.method(Connection.Method.GET)
			.execute()
		;
		
		Profile.forumsPageCookies = forumsLoginForm.cookies();
		
		forumsLoginForm = Jsoup.connect("LINK REDACTED")
			
			.data("login", Profile.forumsUsername)
			.data("register", "0")
			.data("password", Profile.forumsPassword)
			.data("cookie_check", "1")
			.data("_xfToken", "")
			.data("redirect", "LINK REDACTED")
			.cookies(Profile.forumsPageCookies)
			.followRedirects(true)
			.method(Method.POST)
			.execute()
		;
		
		Profile.forumsPageCookies.putAll(forumsLoginForm.cookies());
		
		forumsLoginForm = Jsoup.connect("LINK REDACTED")
			
			.data("code", twoStepCode)
			.data("trust", "1")
			.data("provider", "totp")
			.data("_xfConfirm", "1")
			.data("_xfToken", "")
			.data("remember", "0")
			.data("redirect", "LINK REDACTED")
			.data("save", "Confirm")
			.data("_xfRequestUri", "/login/two-step?redirect=https%3A%2F%2FLINK REDACTED%2F&remember=0")
			.data("_xfNoRedirect", "1")
//			.data("_xfResponseType", "json")
			.cookies(Profile.forumsPageCookies)
//			.ignoreContentType(true)
			.followRedirects(true)
			.method(Method.POST)
			.execute()
		;
		
		Profile.forumsPageCookies.putAll(forumsLoginForm.cookies());
	}
	
	public static void setBanPageUsername(String username) {
		
		Profile.username = username;
	}
	
	public static void setBanPagePassword(String password) {
		
		Profile.password = password;
	}
	
	public static void setForumsUsername(String forumsUsername) {
		
		Profile.forumsUsername = forumsUsername;
	}
	
	public static void setForumsPassword(String forumsPassword) {
		
		Profile.forumsPassword = forumsPassword;
	}
	
	public static void punish(String player, int months, String reason, boolean banOrMute) throws IOException {
		
		String url = "LINK REDACTED";
		
		if (!banOrMute) {
			
			url = "LINK REDACTED";
		}
		
		Jsoup.connect(url)
			.data("username", player)
			.data("length", "" + months)
			.data("reason", reason)
			.cookies(Profile.banPageCookies)
			.post()
		;
	}
	
	public static void postMessage(Report report, String message, boolean lock) throws IOException, NullPointerException {
		
		String url = report.getUrlStr();
		
		Document doc = Jsoup.parse(Jsoup.connect(url)
			.cookies(Profile.forumsPageCookies)
			.get()
			.outerHtml(),
			
			"utf-8")
		;
		
		Element form = doc.getElementById("QuickReply");
		
		Elements inputElements = form.getElementsByTag("input");
		
		Map<String, String> data = new HashMap<String, String>();
		
		for (Element e : inputElements) {
			
			String attributeName = e.attr("name");
			
			if (!(attributeName == null || attributeName.equals("") || attributeName.equals("image") || attributeName.equals("more_options") || attributeName.equals("thumb"))) {
				
				data.put(e.attr("name"), e.attr("value"));
			}
		}
		
		Connection conn = Jsoup.connect(url + "add-reply");
		
		conn.data("message_html", "<p>" + message + "</p>");
		conn.data("submit", "Post Reply");
		
		for (String str : data.keySet()) {
			
			conn.data(str, data.get(str));
		}
		
		if (lock) {
			
			conn.data("_set[discussion_open]", "1");
		}
		
		conn.cookies(Profile.forumsPageCookies).post();
	}
	
	public static void logout() throws IOException {
		
		Document doc = Jsoup.connect("LINK REDACTED")
			.cookies(Profile.forumsPageCookies)
			.get()
		;
		
		Elements elms = doc.getElementsByTag("a");
		
		String url = "";
		
		for (Element e : elms) {
			
			String outerHtml = e.outerHtml();
			
			if (!outerHtml.startsWith("<a href=\"logout")) {
				
				continue;
			}
			
			String href = outerHtml.substring("<a href=\"".length());
			
			url = href.substring(0, href.indexOf("\""));
			
			break;
		}
		
		if (url.equals("")) {
			
			throw new IOException();
		}
		
		Jsoup.connect("LINK REDACTED" + url)
			.cookies(Profile.forumsPageCookies)
			.get()
		;
	}
}