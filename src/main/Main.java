package main;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

import main.customtextdoc.JTextFieldLimit;
import main.profile.Profile;
import main.report.Report;
import main.report.ReportsHandler;
import main.shutdownhook.ShutdownHook;
import main.storage.Storage;
import net.miginfocom.swing.MigLayout;

public class Main {
	
	public static final String HTML_NEW_LINE = "<br />", NEW_LINE = System.getProperty("line.separator");
	
	public static JFrame frame;
	public static JPanel panel;
	
	public static JButton back, skip, punishmentButton, postForumMessage;
	
	public static JScrollPane scrollPane, responseScrollPane;
	public static JEditorPane textPane;
	public static JTextArea responseArea;
	
	public static JTextField name, length, reason, forumPost;
	
	public static JCheckBox ban, mute, lock;
	
	public static ReportsHandler reportsHandler;
	
	public static void main(String[] args) {
		
		String username = "", password = "", fUsername = "", fPassword = "";
		
		boolean manual = true;
		
		if (Storage.hasFile()) {
			
			String response = JOptionPane.showInputDialog("Do you want to manually login?  Enter YES or NO");
			
			if (response == null) {
				
				return;
			}
			
			if (response.equalsIgnoreCase("Yes")) {
				
				manual = true;
			} else if (response.equalsIgnoreCase("No")) {
				
				try {
					
					String[] information = Storage.getInformation();
					
					username = information[0].substring("Username:".length());
					password = information[1].substring("Password:".length());
					fUsername = information[2].substring("fUsername:".length());
					fPassword = information[3].substring("fPassword:".length());
					
					manual = false;
				} catch (FileNotFoundException e1) {
					
					JOptionPane.showMessageDialog(null, "An error occurred when reading your information.  Please login manually!");
					manual = true;
				}
			} else {
				
				JOptionPane.showMessageDialog(null, response + " is not a valid response!");
				return;
			}
		} else {
			
			try {
				
				Storage.createFile();
			} catch (IOException e1) {
				
				e1.printStackTrace();
			}
		}
		
		if (manual) {
			
			username = JOptionPane.showInputDialog("What is your ban page Username?");
			
			if (username == null) {
				
				return;
			}
			
			password = JOptionPane.showInputDialog("What is your ban page Password?");
			
			if (password == null) {
				
				return;
			}
			
			fUsername = JOptionPane.showInputDialog("What is your forums Username?");
			
			if (fUsername == null) {
				
				return;
			}
			
			fPassword = JOptionPane.showInputDialog("What is your forums Password?");
			
			if (fPassword == null) {
				
				return;
			}
			
			if (Storage.hasFile()) {
				
				try {
					
					Storage.write(username, password, fUsername, fPassword);
					JOptionPane.showMessageDialog(null, "Your usernames and passwords have been saved for an easier future login!");
				} catch (IOException e1) {
					
					e1.printStackTrace();
				}
			}
		}
		
		String twoStepAuth = JOptionPane.showInputDialog("What is your two-step auth code?");
		
		if (twoStepAuth == null) {
			
			return;
		}
		
		Main.frame = new JFrame();
		
		MigLayout migLayout = new MigLayout();
		Main.panel = new JPanel(migLayout);
		Main.panel.setPreferredSize(new Dimension(800, 450));
		
		Main.back = new JButton("Back [Coming Soon]");
		
		Main.panel.add(Main.back, "w 100%");
		
		Main.skip = new JButton("Skip");
		
		Main.panel.add(Main.skip, "w 100%, wrap");
		
		Main.scrollPane = new JScrollPane();
		Main.scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		Main.scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		Main.textPane = new JEditorPane();
		Main.textPane.setContentType("text/html");
		Main.textPane.setEditable(false);
		
		Main.scrollPane.getViewport().add(Main.textPane);
		
//		17 is the (number of fields in the side-bar) + 1 so the last field will not be centered.
		Main.panel.add(Main.scrollPane, "h 100%, w 75%, grow, spany 17, spanx 2");
		
		Main.name = Main.createBox("Name", "w 100%, span 2, wrap", 16);
		Main.length = Main.createBox("Length", "w 100%, span 2, wrap", 3);
		Main.reason = Main.createBox("Reason", "w 100%, span 2, wrap", 95);
		
		Main.createDisplay("Ban or Mute", "w 100%, span 2, wrap");
		
		Main.ban = new JCheckBox("Ban");
		Main.ban.setSelected(true);
		Main.panel.add(Main.ban, "w 75%, gapx 3%");
		
		Main.mute = new JCheckBox("Mute");
		Main.mute.setHorizontalTextPosition(SwingConstants.LEFT);
		Main.panel.add(Main.mute, "w 75%, wrap");
		
		Main.punishmentButton = new JButton("Punish");
		Main.panel.add(Main.punishmentButton, "w 100%, span 2, wrap");
		
		Main.createDisplay("Forum Post", "w 100%, span 2, wrap");
		Main.forumPost = Main.createBox("Accepts HTML (<br /> for new line)", "w 100%, span 2, wrap", 350);
		
		Main.lock = new JCheckBox("Lock Thread");
		Main.lock.setSelected(true);
		Main.panel.add(Main.lock, "w 100%, span 2, wrap");
		
		Main.postForumMessage = new JButton("Post Forum Message");
		Main.panel.add(Main.postForumMessage, "w 100%, span 2, wrap");
		
		Main.createDisplay("Response", "w 100%, span 2, wrap");
		
		Main.responseScrollPane = new JScrollPane();
		Main.responseScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		Main.responseScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		Main.responseArea = new JTextArea();
		Main.responseArea.setLineWrap(true);
		Main.responseArea.setWrapStyleWord(true);
		Main.responseArea.setEditable(false);
		
		Main.responseScrollPane.getViewport().add(Main.responseArea);
		
		Main.panel.add(Main.responseScrollPane, "h 50%, w 100%, span 2, wrap");
		
		Main.frame.add(Main.panel);
		Main.frame.pack();
		
		Main.frame.setLocationRelativeTo(null);
		Main.frame.setTitle("Anthony\'s Report Handler");
		Main.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Main.frame.setVisible(true);
		
		Runtime.getRuntime().addShutdownHook(new ShutdownHook());
		
		Main.ban.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if (Main.ban.isSelected()) {
					
					Main.mute.setSelected(false);
					return;
				}
				
				Main.mute.setSelected(true);
			}
		});
		
		Main.mute.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if (Main.mute.isSelected()) {
					
					Main.ban.setSelected(false);
					return;
				}
				
				Main.ban.setSelected(true);
			}
		});
		
		try {
			
			Profile.init(username, password, fUsername, fPassword, twoStepAuth);
		} catch (IOException e2) {
			
			Main.textPane.setText("Something went wrong when logging in!  Please try again!");
			return;
		}
		
		try {
			
			Main.reportsHandler = new ReportsHandler();
			Main.reportsHandler.refill();
			
			List<Report> reports = Main.reportsHandler.getReportsDirectly();
			
			if (reports.size() == 0) {
				
				Main.textPane.setText("There are no reports that need to be checked from page 1 - " + Profile.maxPage + "!  You can have a life!!!");
				return;
			}
			
			Main.textPane.setText(reports.get(0).toString() + Main.HTML_NEW_LINE);
			Main.textPane.setCaretPosition(0);
		} catch (IOException e) {
			
			Main.textPane.setText("Something went wrong while fetching reports.  Please try again!");
			e.printStackTrace();
			return;
		}
		
		Main.skip.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				List<Report> reports = Main.reportsHandler.getReportsDirectly();
				
//				Remove the current report.
				Report prev = reports.remove(0);
				
				if (reports.size() == 0) {
					
					try {
						
						Main.reportsHandler.refill();
					} catch (IOException e1) {
						
						Main.textPane.setText("Something went wrong while fetching reports.  Please try again!");
						e1.printStackTrace();
						return;
					}
					
					reports = Main.reportsHandler.getReportsDirectly();
					
					if (reports.size() == 0) {
						
						Main.textPane.setText("There are no reports that need to be checked from page 1 - " + Profile.maxPage + "!  You can have a life!!!");
						return;
					}
				}
				
				Report next = reports.get(0);
				
				if (next.getUrlStr().equals(prev.getUrlStr())) {
					
					Main.textPane.setText("There are no other reports that need to be checked from page 1 - " + Profile.maxPage + "!  You can have a life!!!");
					return;
				}
				
				Main.textPane.setText(next.toString());
				Main.textPane.setCaretPosition(0);
				
				Main.name.setText("");
				Main.length.setText("");
				Main.reason.setText("");
				Main.forumPost.setText("");
				
				Main.ban.setSelected(true);
				Main.mute.setSelected(false);
				
				Main.lock.setSelected(true);
			}
		});
		
		Main.punishmentButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				String 	name = Main.name.getText(),
						length = Main.length.getText(),
						reason = Main.reason.getText()
				;
				
				if (!(name.equals("") || length.equals("") || reason.equals(""))) {
					
					boolean banOrMute = Main.ban.isSelected();
					
					if (Main.isInteger(length)) {
						
						int months = Integer.valueOf(length);
						
						if (months > 12) {
							
							months = 999;
						}
						
						try {
							
							Profile.punish(name, months, reason, banOrMute);
							Main.responseArea.append((banOrMute ? "Banned " : "Muted ") + name + " for " + months + " months for the reason " + reason + Main.NEW_LINE);
							
							Main.name.setText("");
							Main.length.setText("");
							Main.reason.setText("");
						} catch (IOException e1) {
							
							Main.responseArea.append("[!!!] ERR: " + e1.getClass().getName() + " " + e1.getMessage() + Main.NEW_LINE);
							Main.responseArea.append("[!!!] An error has occurred!  Please check https://bans.cubecraft.net/?username=" + name + " to check if the " + (banOrMute ? "ban" : "mute") + " has been placed!" + Main.NEW_LINE);
						}
					} else {
						
						Main.responseArea.append("The " + (banOrMute ? "ban" : "mute") + " length " + length + " is not a number from 1-12 (or 999 for perm-ban)!" + Main.NEW_LINE);
						Main.responseArea.append("[!!!] The " + (banOrMute ? "ban" : "mute") + " for " + name + " was CANCELLED!" + Main.NEW_LINE);
					}
				}
			}
		});
		
		Main.postForumMessage.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					
					List<Report> reports = Main.reportsHandler.getReportsDirectly();
					Report currentReport = reports.get(0);
					
					String forumPost = Main.forumPost.getText();
					
					if (forumPost.equals("")) {
						
						forumPost = "Thanks for the report, " + currentReport.getPoster() + " :)";
					}
					
					if (Main.lock.isSelected()) {
						
						forumPost += Main.HTML_NEW_LINE + "-Locked";
					}
					
					try {
						
						Profile.postMessage(currentReport, forumPost, Main.lock.isSelected());
						Main.responseArea.append("You posted the message \"" + forumPost + "\" on the forums!" + Main.NEW_LINE);
						Main.responseArea.append("You " + (Main.lock.isSelected() ? "locked the thread!" : "left the thread open!") + Main.NEW_LINE);
					} catch (IOException e1) {
						
						Main.responseArea.append("[!!!] ERR: " + e1.getClass().getName() + " " + e1.getMessage() + Main.NEW_LINE);
						Main.responseArea.append("[!!!] An error has occurred!  Please check " + currentReport.getUrlStr() + " to check if your message was posted and to check if the thread is locked!" + Main.NEW_LINE);
					}
					
					
//					Remove the current report.
					reports.remove(0);
					
					if (reports.size() == 0) {
						
						try {
							
							Main.reportsHandler.refill();
						} catch (IOException e1) {
							
							Main.textPane.setText("Something went wrong while fetching reports.  Please try again!");
							e1.printStackTrace();
							return;
						}
						
						reports = Main.reportsHandler.getReportsDirectly();
						
						if (reports.size() == 0) {
							
							Main.textPane.setText("There are no reports that need to be checked from page 1 - " + Profile.maxPage + "!  You can have a life!!!");
							return;
						}
					}
					
					Report next = reports.get(0);
					
					Main.textPane.setText(next.toString());
					Main.textPane.setCaretPosition(0);
					
					Main.name.setText("");
					Main.length.setText("");
					Main.reason.setText("");
					Main.forumPost.setText("");
					
					Main.ban.setSelected(true);
					Main.mute.setSelected(false);
					
					Main.lock.setSelected(true);
				}
			}
		);
	}
	
	private static JTextField createBox(String name, String migSpecifications, int boxLimit) {
		
		Main.createDisplay(name, migSpecifications);
		
		JTextField field = new JTextField();
		field.setEditable(true);
		field.setDocument(new JTextFieldLimit(boxLimit));
		
		Main.panel.add(field, migSpecifications);
		
		return field;
	}
	
	private static void createDisplay(String name, String migSpecifications) {
		
		JTextField display = new JTextField(name);
		display.setHorizontalAlignment(JTextField.CENTER);
		display.setEditable(false);
		display.setBorder(null);
		
		Main.panel.add(display, migSpecifications);
	}
	
	/**
	 * Credit to http://stackoverflow.com/questions/5439529/determine-if-a-string-is-an-integer-in-java
	 */
	public static boolean isInteger(String s) {
		
		return Main.isInteger(s,10);
	}
	
	/**
	 * Credit to http://stackoverflow.com/questions/5439529/determine-if-a-string-is-an-integer-in-java
	 */
	public static boolean isInteger(String s, int radix) {
		
		if (s.isEmpty()) {
			
			return false;
		}
		
		for (int i = 0; i < s.length(); i++) {
			
			if (Character.digit(s.charAt(i), radix) < 0) {
				
				return false;
			}
		}
		
		return true;
	}
}